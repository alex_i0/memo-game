const cardsImages = ["redsoil.jpg", "spacedonut.jpg", "cyber.jpg", "cyber.jpg", "mistyavenue.jpg", "neoguy.jpg", "spacedonut.jpg", "militia.jpg", "redsoil.jpg", "neoguy.jpg", "militia.jpg", "mistyavenue.jpg"];

const cardsCreator = (amount) => {
	const c = [];

	for (let i = 0; i < amount; i++) {
		c[i] = document.getElementById(`c${i}`);
		c[i].addEventListener("click", () => revealCard(i));
	}
}

const shuffleArray = (array) => {
	for (let i = array.length - 1; i > 0; i--) {
		const j = Math.floor(Math.random() * (i + 1));
		[array[i], array[j]] = [array[j], array[i]];
	}
}

cardsCreator(12);
shuffleArray(cardsImages);

let oneVisible = false;
let turnCounter = 0;
let visible_nr;
let lock = false;
let pairsLeft = 6;

const revealCard = (nr) => {
	if (visible_nr === nr) {
		return null;
	}
	let currentCard = document.getElementById(`c${nr}`);
	const opacityValue = getComputedStyle(currentCard)['opacity'];

	if (opacityValue != 0 && lock == false) {
		lock = true;

		const image = "url(img/" + cardsImages[nr] + ")";

		currentCard.style.backgroundImage = image;

		if (currentCard.classList)
			currentCard.classList.add('cardA');
		else
			currentCard.className += ' ' + 'cardA';

		if (currentCard.classList)
			currentCard.classList.remove('card');

		if (oneVisible == false) {
			oneVisible = true;
			visible_nr = nr;
			lock = false;
		}
		else {

			if ((cardsImages[visible_nr] === cardsImages[nr]) && (visible_nr !== nr)) {
				setTimeout(() => hide2Cards(nr, visible_nr), 750);
				turnCounter++;
			}
			else if (visible_nr !== nr) {
				setTimeout(() => restore2Cards(nr, visible_nr), 1000);
				turnCounter++;
			}

			document.getElementById('score').innerHTML = `Turn counter: ${turnCounter}`;
			oneVisible = false;
		}
	}
}

//remove cards from deck
const hide2Cards = (nr1, nr2) => {
	document.getElementById(`c${nr1}`).style.opacity = 0;
	document.getElementById(`c${nr2}`).style.opacity = 0;

	pairsLeft--;

	if (pairsLeft == 0) {
		document.getElementById('board').innerHTML = `<h1>You win!<br>Done in ${turnCounter} turns</h1>`;
	}

	lock = false;
}

const restore2Cards = (nr1, nr2) => {
	const cards = [nr1, nr2];

	cards.forEach(card => {
		const card1 = document.getElementById(`c${card}`);
		card1.style.backgroundImage = 'url(img/emblem.png)';

		if (card1.classList)
			card1.classList.add('card');
		else
			card1.className += ' ' + 'card';

		if (card1.classList)
			card1.classList.remove('cardA');
	});
	lock = false;
}